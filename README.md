# Systems Integration Proof of Concept - AMQP Workers


## Provisioning Automation using Workers
The following technical configuration changes will be automated using workers consuming RabbitMQ Queues.

- Add user record to RADIUS
- Add user record in Cisco Service Control platform
- Update credentials in RADIUS
- Update package in Cisco Service Control platform
- Assign package in Cisco Service Control platform
- Update package in Cisco Service Control platform

