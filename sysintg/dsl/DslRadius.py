#!/usr/bin/python
# -*- coding: utf-8 -*-

#import MySQLdb as mysql
import MySQLdb
import sys
import json
import pika
import syslog

# for generating random passwords
import os, random, string
import requests 

'''
{u'username': u'alikhalil', u'service_commission_date': u'2012-12-20', u'service_end_date': u'2012-12-20', u'so_type': u'New', u'circuit_id': u'17162499', u'subject': u'Residential BItstream 4Mbps (25GB-TH) - staff', u'contract_term': u'0', u'accountname': u'Ali Khalil', u'account_no': u'ACC1930', u'contactid': u'23330', u'salesorderid': u'29287', u'products': [{u'product_type': u'Bitstream', u'entitytype': u'Products', u'productname': u'Residential BItstream 4Mbps (25GB-TH) - staff', u'package_id': u'BRAS_706', u'salesorder_id': u'29287', u'productid': u'23547'}], u'enable_recurring': u'1', u'potentialid': u'29286', u'salesorder_no': u'SO4910', u'sostatus': u'Active', u'accountid': u'25135'}
'''

#
# TODO: Create a Class based script
#

class DslRadius(object):
	'''
	Interact with RADIUS Server based on messages
	'''
	
	def __init__(self):
		# MySQL related configuration parameters
		# MySQL server details to be added in to config_db.py file
		# 'localhost', 'daloradius', 'daloradius', 'radius' 
		self.mysql_host = 'localhost'
		self.mysql_user = 'daloradius'
		self.mysql_passwd = 'daloradius'
		self.mysql_db = 'radius'
		#self.prime()


	def __del__(self):
		'''
		Cleanup
		'''
		# Close the database connection before exiting program
		try:
			#print "___del___"
			self.db.close()
			self.log('''Ending Database instance''')
			return True
		except:
			self.log( '''Problem closing connection to database.''' )
			self.log( str( sys.exc_info() ) )
			return False
		
	def prime(self):
		'''
		Process priming functions to load basic data in to object
		'''
		#print "___prime___"
		try:
			self.db = self.radiusDB()
			#self.getTagTranslations()
			#self.genTableDefinitions()
			return True
		except:
			self.log( '''Cannot prime the database object.''' )
			self.log( str( sys.exc_info() ) )
			return False
		
		
	def radiusDB(self):
		'''
		Database object
		'''
		#print "___radiusDB___"
		try:
			db = MySQLdb.connect( host=self.mysql_host, user=self.mysql_user, passwd=self.mysql_passwd, db=self.mysql_db)
			return db
		except MySQLdb.Error, e:
			self.log( '''Problem connecting to database''' )
			self.log( "Error %d: %s" % (e.args[0], e.args[1]) )
			self.log( str( sys.exc_info() ) )
			return False
		
		
	def gen_password(self, length=10):
		'''
		http://stackoverflow.com/questions/7479442/high-quality-simple-random-password-generator
		'''
		#print "___gen_password___"
		#chars = string.ascii_letters + string.digits + '!@#$%^&*()'
		chars = string.ascii_letters + string.digits 
		random.seed = (os.urandom(1024))
		return ''.join(random.choice(chars) for i in range(length))
	
	
	def process_create_subscriber( self, ch, method, properties, body ):
		'''
		Read the data payload and use it to perform processing
		'''
		#print "___process_create_subscriber___"
		data = json.loads(body)
		
		# Initialize info dictionary to be updated in next few lines
		info = {
			'group': 'business-dynamic',
			'group_priority': 0,
			'username': data['username'],
			'operator': 'sysintg',
			'static_ip': False,
			'package_id': False,
			}
		
		groups = [ 'business-static', 'business-dynamic', 'residential-dynamic']
	
		# Check for Package ID and Static IP 
		for product in data['products']:
			if product['product_type'] == 'Static IP':
				info['static_ip'] = product['comment']
				info['group'] = 'business-static'
			if product['package_id']:
				info['package_id'] = product['package_id']
	
		# DUMMY CUSTOMER INFORMATION
		_info = {
			'group': 'business-dynamic',
			'group_priority': 0,
			'username': 'SYSINTG_%s'%(self.gen_password(5)),
			'operator': 'sysintg',
			'static_ip': '80.88.252.250',
			'package_id': False,
			}
	
	
		self.log( ''' [^] User information received: %s'''%(info) )
	
		if not self.check_existing(info):
			self.create_mysqldb( info )
			self.create_scabb( info )
			
		#create_mysqldb( info )
	
		ch.basic_ack(delivery_tag=method.delivery_tag)
	
		
		
	def check_existing( self, info ):
		'''
		Check database to see if user exists. 
		'''
		try:
			#print "___check_existing___"
			username = info['username']
			
			SQL_statement = '''SELECT COUNT(*) FROM radcheck WHERE username="%s";'''%(username)
	
			
			#con = mysql.connect( 'localhost', 'daloradius', 'daloradius', 'radius' )
			#cur = con.cursor()
			cursor = self.db.cursor()
			cursor.execute(SQL_statement)
			#con.commit()
			
			exists = cursor.fetchone()[0]
			#print exists
			self.db.commit()
			cursor.close()
            
			self.log( " [?] Processing requested for %s"%(username) )
			
			if exists > 0:
				self.log( '''   [-] User exists in database - %s'''%(username) )
				return True
			else:
				self.log( '''   [>] New user detected - %s'''%(username) )
				return False
			
		except MySQLdb.Error, e:
			#con.rollback()
			self.log( "Error %d: %s" % (e.args[0], e.args[1]) )
			self.log( str( sys.exc_info() ) )
			return True
				
	
	
	
	def create_mysqldb( self, info ):
		try:
			#print "___create_mysqldb___"
			username = info['username']
			password = self.gen_password()
			group = info['group']
			operator = info['operator']
			static_ip = info['static_ip']
			group_priority = info['group_priority']
	
			
			SQL_statement = ''
			
			# RADCHECK
			SQL_statement = SQL_statement + '''INSERT INTO radcheck(username, attribute, op, value ) VALUES( "%s", "%s", "%s", "%s" );\n'''%(
						username,
						'Cleartext-Password',
						':=',
						password
						)
			
			# USERBILLINFO
			SQL_statement = SQL_statement + '''INSERT INTO userbillinfo( username, creationby, updateby ) VALUES( "%s", "%s", "%s" );\n'''%(
						username,
						operator,
						operator
						)
			
			# RADREPLY
			if static_ip:
				SQL_statement = SQL_statement + '''INSERT INTO radreply( username, attribute, op, value ) VALUES( "%s", "%s", "%s", "%s" );\n'''%(
						username,
						'Framed-IP-Address',
						'=',
						static_ip
						)
			
			# RADUSERGROUP
			SQL_statement = SQL_statement + '''INSERT INTO radusergroup( username, groupname, priority ) VALUES( "%s", "%s", %s );\n'''%(
						username,
						group,
						group_priority
						)
			
			# USERINFO		
			SQL_statement = SQL_statement + '''INSERT INTO userinfo( username, enableportallogin, creationby, updateby ) VALUES( "%s", %s, "%s", "%s" );\n'''%(
						username,
						'0',
						operator,
						operator
						)
			
			#print SQL_statement
			
			#con = mysql.connect('localhost', 'daloradius', 'daloradius', 'radius')
			cursor = self.db.cursor()
			cursor.execute(SQL_statement)
			cursor.close()
			self.db.commit()
			#con.commit()
			self.log( "   [+] User %s successfully added to database with password %s"%(username, password) )
			return True
			#con.close()
		except MySQLdb.Error, e:
			self.db.rollback()
			self.log( "Error %d: %s" % (e.args[0], e.args[1]) )
			self.log( str( sys.exc_info() ) )
			return False


				
	def create_scabb(self, info):
		try:
			username = info['username']
			package = info['package_id']
			url = 'http://192.168.6.5:49000/'
			data = {'action': 'adduser', 'subscriber': username, 'pkgid': package }
			response = requests.get( url, params=data )
			self.log( "   [+] Data: %s"%(data) )
			self.log( "   [+] Response: %s"%(response) )
			#r = json.loads(response.content.rstrip())
			self.log( "   [+] User %s successfully added to Subscriber Manager."%(info['username']) )
			return True
		except:
			self.log( str( sys.exc_info() ) )
			return False

	def process_update_scabb( self, ch, method, properties, body ):
		'''
		Read the data payload and use it to perform processing
		'''
		data = json.loads(body)
		
		# Initialize info dictionary to be updated in next few lines
		info = {
			'username': data['username'],
			'package_id': data['package_id'],
			}
	
		self.log( ''' [^] User information received for package update: %s'''%(info) )
	
		if self.check_existing(info):
			self.update_scabb( info )
			
		ch.basic_ack(delivery_tag=method.delivery_tag)
		
	def update_scabb(self, info):
		try:
			username = info['username']
			package = info['package_id']
			url = 'http://192.168.6.5:49000/'
			data = {'action': 'setpkg', 'subscriber': username, 'pkgid': package }
			response = requests.get( url, params=data )
			self.log( "   [+] Data: %s"%(data) )
			self.log( "   [+] Response: %s"%(response) )
			#r = json.loads(response.content.rstrip())
			self.log( "   [+] Package successfully updated for user %s"%(info['username']) )
			return True
		except:
			self.log( str( sys.exc_info() ) )
			return False


	def process_topup_scabb( self, ch, method, properties, body ):
		'''
		Read the data payload and use it to perform processing
		'''
		data = json.loads(body)
		
		# Initialize info dictionary to be updated in next few lines
		info = {
			'username': data['username'],
			'quota_amount': data['quota_amount'],
			}
	
		self.log( ''' [^] User information received for package update: %s'''%(info) )
	
		if self.check_existing(info):
			self.topup_scabb( info )
			
		ch.basic_ack(delivery_tag=method.delivery_tag)
		

	def topup_scabb(self, info):
		try:
			username = info['username']
			quota_amount = info['quota_amount']
			url = 'http://192.168.6.5:49000/'
			data = {'action': 'topup', 'subscriber': username, 'quota_amount': quota_amount }
			response = requests.get( url, params=data )
			self.log( "   [+] Data: %s"%(data) )
			self.log( "   [+] Response: %s"%(response) )
			#r = json.loads(response.content.rstrip())
			self.log( "   [+] Quota successfully updated for user %s by %s KB"%(info['username'], info['quota_amount']) )
			return True
		except:
			self.log( str( sys.exc_info() ) )
			return False


	def process_reset_password( self, ch, method, properties, body ):
		'''
		Read the data payload and use it to perform processing
		'''
		#print "___process_create_subscriber___"
		#print body
		data = json.loads(body)
		
		
		# Initialize info dictionary to be updated in next few lines
		info = {
			'username': data['username'],
			'password': self.gen_password(5),
			}
		
		# DUMMY CUSTOMER INFORMATION
		#temp_password=self.gen_password(5)
		#print temp_password
		#_info = {
		#	'username': 'SYSINTG_%s'%(temp_password),
		#	'password': '%s'%(temp_password),
		#	}
	
	
		self.log( ''' [^] User information received for password reset: %s'''%(info) )
	
		if self.check_existing(info):
			self.reset_password( info )
			
		#create_mysqldb( info )
	
		ch.basic_ack(delivery_tag=method.delivery_tag)
	
		
	def reset_password(self, info):
		'''
		Reset the password and display notification
		'''
		try:
			#print "___create_mysqldb___"
			username = info['username']
			#password = self.gen_password()
			password = info['password']
			
			SQL_statement = ''
			# RADCHECK
			SQL_statement = SQL_statement + '''UPDATE radcheck SET value="%s" WHERE username="%s" AND attribute="%s";\n'''%(
						password,
						username,
						'Cleartext-Password'
						)
			
			#print SQL_statement
			
			#con = mysql.connect('localhost', 'daloradius', 'daloradius', 'radius')
			cursor = self.db.cursor()
			cursor.execute(SQL_statement)
			cursor.close()
			self.db.commit()
			#con.commit()
			self.log( "   [+] User %s successfully updated with new password %s"%(username, password) )
			return True
			#con.close()
		except MySQLdb.Error, e:
			self.db.rollback()
			self.log( "Error %d: %s" % (e.args[0], e.args[1]) )
			self.log( str( sys.exc_info() ) )
			return False
		

				
	def log(self, message):
		'''
		Output error to either stdout or syslog
		'''
		#print "___log___"
		# Method options stdout or syslog
		methods = [ 'stdout', 'syslog' ]
		method = methods[0]
		try:
			if method == 'syslog':
				syslog.syslog( str(message) )
			else:
				print message
			return True
		except:
			print '''Error - cannot log messages.'''
			print str( sys.exc_info() )
			return False
		
	def messaging(self, host, virtual_host, exchange, queue):
		try:
			#print "___messaging___"
			#connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', virtual_host='tempapp'))
			connection = pika.BlockingConnection(pika.ConnectionParameters(host=host, virtual_host=virtual_host))
			channel = connection.channel()
		
			#result = channel.queue_declare(queue='bras-radius')
			result = channel.queue_declare(queue=queue)
			queue_name = result.method.queue
		
			#channel.queue_bind(exchange='ta_fanout', queue=queue_name)
			channel.queue_bind(exchange=exchange, queue=queue_name)
		
			self.log( ' [*] MODULE: ADD USER INFO IN RADIUS DATABASE' ) 
			self.log( ' [*] Waiting for information. To exit press CTRL+C' )
			self.log( '   [*] Queue: %s'%(queue_name) )
			
			queue_to_method = {
								'ResetPassword': self.process_reset_password,
								'ProvisionSubscriber': self.process_create_subscriber,
								'UpdatePackage': self.process_update_scabb,
								'TopupQuota': self.process_topup_scabb,
							}
			channel.basic_consume(queue_to_method[queue_name],
								  queue=queue_name,
								  no_ack=False)
			channel.start_consuming()

			return True
		except:
			self.log( "Error %s"%( str(sys.exc_info()) ) )
			return False
			
		
