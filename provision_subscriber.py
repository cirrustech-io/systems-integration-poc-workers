#!/usr/bin/env python
#
# Run a worker to reset passwords for DSL subscribers
#
# Ali Khalil, 2014-09-14
#

from sysintg.dsl.DslRadius import DslRadius

if __name__ == '__main__':
    rad = DslRadius()
    try:
        rad.prime()
        rad.messaging( 'mq.2connectbahrain.com', 'poc', 'dsl', 'ProvisionSubscriber' )
    except:
        print "not working!!!!!!!!"
    

